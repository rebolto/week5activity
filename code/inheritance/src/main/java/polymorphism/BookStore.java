package polymorphism;

public class BookStore {
    public static void main(String[]args){
        Book[] books = new Book[5];
        books[0]=new Book("Building a car with toothpicks", "Jamal");
        books[1]=new ElectronicBook("Tips and Tricks to Skateboarding", "Tony Hawk", 66);
        books[2]=new Book("Building a car with marshmallows", "Steve Jobs");
        books[3]=new ElectronicBook("Tips and Tricks to Sculpting with Clay", "Picasso", 90);
        books[4]=new ElectronicBook("The Reasoning behind Noah's Ark", "J.K Rolling", 66);
        for(int i =0;i<books.length;i++){
            System.out.println(books[i]);
        }
        ElectronicBook b = (ElectronicBook)books[1];
        System.out.println(b.getNumberBytes());
        ElectronicBook b2 = (ElectronicBook)books[0];
        System.out.println(b2.getNumberBytes());
    }
}
