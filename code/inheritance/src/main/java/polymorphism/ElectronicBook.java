package polymorphism;

public class ElectronicBook extends Book {
    private int numberBytes;

    public int getNumberBytes() {
        return this.numberBytes;
    }

    public ElectronicBook(String title, String author, int numberBytes) {
        super(title, author);
        this.numberBytes = numberBytes;

    }

    public String toString() {
        return "Title of Book: " + this.title + ", Author of Book: " + this.getAuthor() + " Size of File: "
                + this.numberBytes;
    }
}
